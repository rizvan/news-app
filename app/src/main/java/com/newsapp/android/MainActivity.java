package com.newsapp.android;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.newsapp.android.databinding.ActivityMainBinding;
import com.newsapp.android.service.NewsServiceClient;
import com.newsapp.android.ui.NewsFragment;
import com.newsapp.android.ui.TopNewsFragment;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private final FragmentManager fragmentManager = getSupportFragmentManager();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setupToolbar();
        TopNewsFragment headlinesFragment = TopNewsFragment.topNewsInstance();
        //NewsFragment newsFragment = NewsFragment.newInstance(NewsServiceClient.Category.general);
        fragmentManager.beginTransaction()
                .add(R.id.fragment_container, headlinesFragment)
                .commit();
    }

    private void setupToolbar() {
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.app_name));
            //Remove trailing space from toolbar
            binding.toolbar.setContentInsetsAbsolute(10, 10);
        }
    }
}
