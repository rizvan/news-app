package com.newsapp.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.material.tabs.TabLayout;
import com.newsapp.android.R;
import com.newsapp.android.adapters.ViewPagerAdapter;
import com.newsapp.android.databinding.FragmentTopnewsBinding;
import com.newsapp.android.service.NewsServiceClient;

public class TopNewsFragment extends Fragment {
    private final String[] categories = {
            NewsServiceClient.Category.general.name(),
            NewsServiceClient.Category.business.name(),
            NewsServiceClient.Category.sports.name(),
            NewsServiceClient.Category.health.name(),
            NewsServiceClient.Category.entertainment.name(),
            NewsServiceClient.Category.technology.name(),
            NewsServiceClient.Category.science.name(),
    };
    private final int[] categoryIcons = {
            R.drawable.ic_headlines,
            R.drawable.nav_business,
            R.drawable.nav_sports,
            R.drawable.nav_health,
            R.drawable.nav_entertainment,
            R.drawable.nav_tech,
            R.drawable.nav_science
    };

    private FragmentTopnewsBinding binding;

    private TopNewsFragment(){}

    public static TopNewsFragment topNewsInstance(){ return new TopNewsFragment();}


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_topnews, container, false);

        ViewCompat.setElevation(binding.tablayoutHeadlines, getResources().getDimension(R.dimen.tab_layout_elevation));

        if (getActivity() != null) {
            ViewPagerAdapter viewPager = new ViewPagerAdapter(getChildFragmentManager(), categories);
            binding.viewpagerHeadlines.setAdapter(viewPager);
            binding.tablayoutHeadlines. setupWithViewPager(binding.viewpagerHeadlines);
            setupTabIcons();
        }
        return this.binding.getRoot();
    }

    private void setupTabIcons() {
        TabLayout.Tab tab;
        for (int i = 0; i < categories.length; i++) {
            tab = binding.tablayoutHeadlines.getTabAt(i);
            if (tab != null) {
                tab.setIcon(categoryIcons[i]).setText(categories[i]);
            }
        }
    }

}
